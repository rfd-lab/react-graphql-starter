# React GraphQL fullstack boilerplate

## What is GraphQL

A spec that describes a declarative query language that your clients can use to ask an API  for the exact data they want.

This is archieved by creating a strongly typed Schema for your API, ultimate flexibility in how your API can resolve data, and client queries validated against your Schema.

## Server-side

* Type Definitions
* Resolvers
* Query Definitions
* Mutation Definitions
* Composition
* Schema

## Client-side

* Queries
* Mutations
* Fragments

